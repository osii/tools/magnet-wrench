# Magnet Wrench

3D printed "wrench" to handle the long (50×12×12mm³) magnets as used in e.g. v2.1.0 of the [30cm Halbach Magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet/).

The handle suits a 11mm spanner.

For a quick 3D view see [here](/res/3d/wrench.stl) (NOTE that this shows potentially an **old** version of the wrench, but you get the point; for STL/STP exports please see the respective [release notes](https://gitlab.com/osii/tools/magnet-wrench/-/releases)).

