# Manufacturing Instructions

## Printing the Parts

**Print with:**

- ABS
- 5cm wall thickness
- 100% infill

**Orientations:**

| Part     | Print Orientation                             | Comment                                                        |
|----------|-----------------------------------------------|----------------------------------------------------------------|
| `wrench` | x-axis parallel to bed (north mark facing up) | otherwise the layers will break/separate when using the wrench |
| `holder` | x-axis normal to bed; tongue pointing away from bed         | for better mechanical properties of the holder|


## Assembling the Parts

superglue on all sides of the tongue + the back side of the handle facing the holder

like this:

![](/res/img/glue-parts.jpg)

